import Vue from 'vue'
import App from './App.vue'
import ROSLIB from 'roslib'
Object.defineProperty(Vue.prototype,"$ROSLIB", {value: ROSLIB});
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
